const MineController = require('./controllers/MineController.js')

const express = require('express')

const routes = express.Router()

routes.get('/', (req, res) => {
  res.status(200)
  res.json({
    status: 'online',
  })
})

routes.get('/mine/check/:url/:port/:version', MineController.index)

module.exports = routes
